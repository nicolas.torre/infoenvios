import React, { useState } from 'react'
import { useQuery } from 'react-apollo'
import useProduct from 'vtex.product-context/useProduct'
import infoEnvios from './infoEnvios.gql'

const convertirDiasHabiles = (abreviatura) => {
  const dias = parseInt(abreviatura);
  if (!isNaN(dias)) {
    return `${dias} día${dias !== 1 ? 's' : ''} hábil${dias !== 1 ? 'es' : ''}`;
  }
  return 'Abreviatura no válida';
};

const InfoEnvios = () => {
  const { product } = useProduct()
  const [codigoPostal, setCodigoPostal] = useState('')
  const [searchClicked, setSearchClicked] = useState(false)

  const { data, loading, error, refetch } = useQuery(infoEnvios, {
    variables: {
      id: product?.items[0]?.itemId,
      quantity: "1",
      postalCode: codigoPostal
    },
    ssr: false,
    skip: !searchClicked // Saltar la consulta si searchClicked es false
  })

  const handleInputChange = (e) => {
    setCodigoPostal(e.target.value)
  }

  const handleSearch = async () => {
    setSearchClicked(true)
    try {
      await refetch()
    } catch (error) {
      console.error('Error fetching delivery options:', error)
    }
  }

  if (!product) {
    return (
      <div>
        <span>There is no product context.</span>
      </div>
    )
  }

  if (data) {
    console.log('Datos de la consulta:', data);
  }

  return (
    <div>
                <h5>Calcular Envío:</h5>

      <input
        type="text"
        value={codigoPostal}
        onChange={handleInputChange}
        placeholder="Ingrese su código postal"
      />
      <button onClick={handleSearch}>Buscar</button>

      {loading && <> Cargando...</>}

      {error && (
        <div>
          
        </div>
      )}

      {data && data.shippingSLA.deliveryOptions && data.shippingSLA.deliveryOptions.length > 0 && (
        <div>
          <h5>Opciones de Envío:</h5>
          <ul className='listaOpcionesEnvio' style={{listStyle: "none", padding: "0"}}>
            {data.shippingSLA.deliveryOptions.map((option, index) => (
              <li key={index} style={{borderBottom: "1px solid #6b0fd22b", marginBottom: "20px"}}>
                <p className='canal' style={{fontSize: "0.8em", lineHeight: 0, marginBottom: "20px"}}>{option.deliveryChannel}</p>
                <p className='nombre' style={{lineHeight: 0, marginBottom: "20px"}}>{option.id}</p>
                <p className='precio' style={{lineHeight: 0}}>{option.price === 0 ? "Gratis" : "$"+ option.price.toString().slice(0, -2)}</p>
                <p className='dias' style={{color: "#6b0fd2"}}>Hasta {convertirDiasHabiles(option.estimate)}</p>
              </li>
            ))}
          </ul>
        </div>
      )}

      {data && data.shippingSLA.deliveryOptions && data.shippingSLA.deliveryOptions.length === 0 && (
        <div>
          <p>No hay opciones de envío disponibles para este producto.</p>
        </div>
      )}
    </div>
  )
}

export default InfoEnvios
